module github.com/spkaeros/rscgo

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/gobwas/httphead v0.0.0-20180130184737-2c6c146eadee // indirect
	github.com/gobwas/pool v0.2.0 // indirect
	github.com/gobwas/ws v1.0.2
	github.com/jessevdk/go-flags v1.4.0
	github.com/mattn/anko v0.1.2
	github.com/mattn/go-sqlite3 v1.11.0
	go.uber.org/atomic v1.5.0
	golang.org/x/crypto v0.0.0-20191106202628-ed6320f186d4
)
